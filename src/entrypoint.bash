#!/bin/bash
# Variables
UNIFI_DIR="/usr/lib/unifi/"
UNIFI_LOGS="${UNIFI_DIR}/logs"

# Applications
JAVA="$(which java)"

# Start Application
${JAVA} \
    -Dfile.encoding=UTF-8 \
    -Djava.awt.headless=true \
    -Dapple.awt.UIElement=true \
    -Dunifi.core.enabled=${UNIFI_CORE_ENABLED} \
    -Dunifi.mongodb.service.enabled=${UNIFI_MONGODB_SERVICE_ENABLED} \
    ${UNIFI_JVM_OPTS} \
    -XX:+ExitOnOutOfMemoryError \
    -XX:+CrashOnOutOfMemoryError \
    -XX:ErrorFile=${UNIFI_LOGS}/logs/hs_err_pid%p.log \
    -Xlog:gc:${UNIFI_LOGS}/gc.log:time:filecount=2,filesize=5M \
    --add-opens java.base/java.lang=ALL-UNNAMED \
    --add-opens java.base/java.time=ALL-UNNAMED \
    --add-opens java.base/sun.security.util=ALL-UNNAMED \
    --add-opens java.base/java.io=ALL-UNNAMED \
    --add-opens java.rmi/sun.rmi.transport=ALL-UNNAMED \
    -jar /usr/lib/unifi/lib/ace.jar start