# unifi-console

This Image contains unifi-console. 
[Further Documentation in German.](https://docs.pazdzewicz.de/docker/unifi/2023/09/19/unifi-selfhosted-docker.html)

## Parent image

- `debian:bullseye`

## Dependencies

None

## Entrypoint & CMD

```
#!/bin/bash
/usr/bin/java \
    -Dfile.encoding=UTF-8 \
    -Djava.awt.headless=true \
    -Dapple.awt.UIElement=true \
    -Dunifi.core.enabled=${UNIFI_CORE_ENABLED} \
    -Dunifi.mongodb.service.enabled=${UNIFI_MONGODB_SERVICE_ENABLED} \
    $UNIFI_JVM_OPTS \
    -XX:+ExitOnOutOfMemoryError \
    -XX:+CrashOnOutOfMemoryError \
    -XX:ErrorFile=/usr/lib/unifi/logs/hs_err_pid%p.log \
    -Xlog:gc:/var/log/gc.log:time:filecount=2,filesize=5M \
    --add-opens java.base/java.lang=ALL-UNNAMED \
    --add-opens java.base/java.time=ALL-UNNAMED \
    --add-opens java.base/sun.security.util=ALL-UNNAMED \
    --add-opens java.base/java.io=ALL-UNNAMED \
    --add-opens java.rmi/sun.rmi.transport=ALL-UNNAMED \
    -jar /usr/lib/unifi/lib/ace.jar start
```

## Functions

NA

## Build Image

### Automated Build

Through the new build pipeline all of our Docker images are built via Gitlab CI/CD and pushed to the Gitlab Container Registry. You usually don't need to build the image on your own, just push your commits to the git. The Image will be tagged after your branch name.

After the automated build is finished you can pull the image:

```
docker pull registry.gitlab.com/pazdzewicz_docker/unifi-console:BRANCH
```

All images also have a master tag, just replace the BRANCH with "master". The "latest" Tag is only an alias for the "master" tag.

### Manual Build

You can also build the Docker image on your local pc:

```
docker build -t "myunifi-console:latest" .
```

### Build Options

Build Options are Arguments in the Dockerfile (`--build-arg`):

- None

### Security Check during Build

Before we release Docker Containers into the wild it is required to run the Anchore security checks over the Pipeline (this doesn't apply to manual built images). You can download the current artifacts on the Pipelines page (the download drop-down).

## Environment Variables:


## Ports

- `8080/tcp` Device command/control
- `8443/tcp` Web interface + API
- `8843/tcp` HTTPS portal (optional)
- `8880/tcp` HTTP portal (optional)
- `6789/tcp` For mobile throughput test
- `3478/udp` STUN service
- `10001/udp` AP discovery
- `1900/udp` Required for `Make controller discoverable` on L2 network option
- `5514/udp` Remote syslog port

## Usage

```
docker-compose pull
docker-compose up -d
```