FROM debian:bullseye

COPY ./src/entrypoint.bash /entrypoint.bash

RUN chmod +x /entrypoint.bash && \
    apt update -y && \
    apt install -y ca-certificates apt-transport-https wget gnupg2 && \
    echo "deb https://www.ui.com/downloads/unifi/debian stable ubiquiti" | tee /etc/apt/sources.list.d/unifi.list && \
    echo "deb http://repo.mongodb.org/apt/debian buster/mongodb-org/4.4 main" | tee /etc/apt/sources.list.d/mongodb.list && \
    wget -qO - https://dl.ui.com/unifi/unifi-repo.gpg | apt-key add - && \
    wget -qO - https://pgp.mongodb.com/server-4.4.asc | apt-key add - && \
    apt update -y  && \
    apt install -y mongodb-org \
                   openjdk-11-jre \
                   unifi && \
    mkdir -p /config/{data,logs}

ENTRYPOINT [ "/entrypoint.bash" ]